#!/bin/bash
#Check at least 2 args passed
if [ "$#" -lt 2 ]; then
    echo "Must give at least 2 inputs: roi, AIF slice"
    exit 2
fi


#Get tumour and slice from args
export TUMOUR="$1"
export AIF=madym_output/AIF/$2_Auto_AIF.txt

#Run madym DCE
$MADYM_ROOT/madym_DCE --config $MADYM_CONFIG_DIR/madym_ETM_auto_config.txt --roi roi/$TUMOUR -o $TUMOUR --aif $AIF "${@:3}"

#Run python DCE fit viewer
python $PYTHON_TOOLS_DIR/DCE_fit_viewer/DCE_fit_viewer_tool.py madym_output/ETM_auto/$TUMOUR
python $PYTHON_TOOLS_DIR/simple_3D_viewer/simple_3D_viewer_tool.py madym_output/ETM_auto/$TUMOUR 2
