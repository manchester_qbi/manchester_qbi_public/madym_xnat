#!/bin/bash

#Name this study
export STUDY_NAME=exciting_new_study

#Set the specific version of Madym used in this study
export MADYM_ROOT="/usr/madym-v4.12.0/bin"

#Set variable to study root

export MADYM_DATA_DIR=/mnt/qbi/data/studies/$STUDY_NAME
export MADYM_CONFIG_DIR="$PWD/`dirname \"$BASH_SOURCE\"`"

#Set path to python tools - comment out igf not using python viewer
export PYTHON_TOOLS_DIR=~/code/manchester_qbi/public/madym_python/tools

#Add this folder to the path
export PATH=$PATH:$MADYM_CONFIG_DIR

echo '******************************************************'
echo Set Madym environment for study: $STUDY_NAME
echo Using madym version:
$MADYM_ROOT/madym_DCE -v
echo '******************************************************'
