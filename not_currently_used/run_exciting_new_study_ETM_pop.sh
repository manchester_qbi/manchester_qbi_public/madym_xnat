#!/bin/bash

#Check roi argument passed
if [ "$#" -lt 1 ]; then
    echo "Must give at least 1 input: roi"
    exit 2
fi

#Get tumour from args
export TUMOUR="$1"

#Run madym DCE
$MADYM_ROOT/madym_DCE --config $MADYM_CONFIG_DIR/madym_ETM_pop_config.txt --roi roi/$TUMOUR -o $TUMOUR "${@:2}"

#Run python DCE fit viewer
python $PYTHON_TOOLS_DIR/DCE_fit_viewer/DCE_fit_viewer_tool.py madym_output/ETM_pop/$TUMOUR
python $PYTHON_TOOLS_DIR/simple_3D_viewer/simple_3D_viewer_tool.py madym_output/ETM_pop/$TUMOUR 2
