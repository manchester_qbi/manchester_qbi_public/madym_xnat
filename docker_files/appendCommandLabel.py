#!/usr/bin/env python
'''
Serialises one or more JSON command files and appends to the LABEL option in a Docker file
Usage: appendCommandLabel <Dockerfile> <Commandfile1> <Commandfile2> ...
'''

import re
import sys
import json

dockerFile = sys.argv[1]
commandFileList = sys.argv[2:]
 
commandStrList = []
for commandFile in commandFileList:
    print(f'Serializing {commandFile} and appending to {dockerFile}')
    with open(commandFile) as f:
        commandObj = json.load(f)
    commandStr = json.dumps(commandObj) \
                        .replace('"', r'\"') \
                        .replace('$', r'\$')
    commandStrList.append(commandStr)


with open(dockerFile, mode = 'a') as f:
    print(
        'LABEL org.nrg.commands="[{}]"'.format(', \\\n\t'.join(commandStrList)), file=f)

print(f'Complete!')
