#!/bin/sh

#Arg 1 is the name of the docker image we're building, which
#also matches the name of dir containing a Dockerfile.base
#we're going to create the actual Dockerfile from
#
#Arg 2 is the name of the XNAT command JSON file we'll append
#which we're assuming is in ../xnat_commands

#Copy the docker base file to a docker file
cp $1/Dockerfile.base $1/Dockerfile 

#Call appendCommandLabel, first arg is Dockerfile, second in command file
python appendCommandLabel.py $1/Dockerfile ../xnat_commands/$2

#Copy the python dicom module into this folder so it is the build context
cp -r ../dicom ./dicom

#Now build the docker image and push to the QbiRegistry
docker build -t qbimanchester/$1:latest -f $1/Dockerfile .
docker push -a qbimanchester/$1

#Remove the dicom copy
rm -r ./dicom