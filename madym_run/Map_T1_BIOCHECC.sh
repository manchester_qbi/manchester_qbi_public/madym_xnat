#!/bin/bash

#Run madym T1
$MADYM_ROOT/madym_T1 --config /config/madym_T1_VFA_config.txt "$@"

#Run DICOM convert wrapper
python dicom/madym_output_to_dicom.py session_resources/dicom_sort session_resources/dicom_convert output/T1_VFA T1
