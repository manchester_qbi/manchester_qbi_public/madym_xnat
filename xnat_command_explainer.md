# Anatomy of an XNAT command JSON file
XNAT supports a container service plugin (see [here](setup.md) for instructions on setting up) that enables running Docker containers on your XNAT node, to perform custom processing tasks on the data stored on your node. This provides almost limitless flexibility in working with your data on XNAT, however while the concept of the the container service is quite simple, in practice it can be a little (ok, very!) tricky to get working. This explainer aims to get you over that initial hurdle...

For official documentation, see [here](https://wiki.xnat.org/container-service/json-command-definition-122978875.html)

## The concept
First a bit more detail on the concept. This assumes that:

1. You are familiar with XNAT, and it's hierarchy of projects, subjects and sessions for storing data
2. You are familiar with the basics concepts of Docker images and containers

### Running Docker on your local machine
Let's assume we have some processing task we want to perform on data on a local machine, and we have a Docker image `cool_image` that contains the necessary software to perform this task. Let's also assume this task will be performed with a single command line instruction. For example, this could be to call a python script:

    python do_something_cool.py input1 input2

Or alternatively calling a shell script that runs multiple commands:

    bash do_lots_of_cool_things.sh input1 input2

To run this in docker we would call something like


    docker run --rm -v <path_to_data_dir>:/data --entrypoint bash cool_image do_lots_of_cool_things.sh input1 input2

Where 

- `entrypoint` overrides any existing start-up commands that would run in the container, and then the name of the script and any additional inputs are passed as commands after the image name.
- `-v <path_to_data_dir>:/data` provides a mount point so that data on your machine at `<path_to_data_dir>` is available inside the docker container at `/data`, providing both access to the data you are going to process, and allowing any output results created during processing to persist after the container is closed.

Before trying to get the container service working, I would strongly recommend getting an example like the above working on your local machine for your data and Docker image.

### Running the container service on XNAT
Translating the above to XNAT, is conceptually simple. XNAT acts a filesystem, so we just need to provide one or more mounts to this filesystem to access data on XNAT to process and tell it where to save any output results to, and then tell the container service what commands and additional arguments we want to run. However... the tricky bit is we don't get to call a docker command directly like we did on our local machine. Instead, we need to define a special command JSON file, that acts as a go-between to how the user initiates calling the container service via XNAT's browser interface, and how the docker container ultimately gets called. And where our docker command above neatly fitted on a single line, the command JSON file we describe here is over 200 lines long!

The remainder of this post aims to unpack that complexity so you can create your own commands.

## The devil is in the detail: your XNAT command JSON explained
The XNAT command JSON file we present can be found in all it's 200+ line glory [here](xnat_commands/madym_command.json). We will run through this step-by-step, but first describe in more detail the specific task we are running here (though hopefully you can see how this generalises to almost any task you may want to run).

### Defining the tasks to run
We are going to be using `Madym`, our [open-source C++ DCE-MRI toolkit](), and in particular running something similar to the *Using Madym in a formal study* [example](https://gitlab.com/manchester_qbi/manchester_qbi_public/madym_cxx/-/wikis/study_analysis). So the three tasks we are going to perform for a given dataset are:

1. Sorting through the DICOM files, checking we have the series we expect and preparing for them to be converting into NIFTI/XTR format for further processing

2. Converting the DICOM files into NIFTI/XTR format, organised as expected for Madym

3. Mapping T1 from the set of variable flip-angle images generated in stage 2

For this, will be using the Madym tool [`madym_DicomConvert`](https://gitlab.com/manchester_qbi/manchester_qbi_public/madym_cxx/-/wikis/madym_dicom) for steps 1 and 2, and [`madym_T1`](https://gitlab.com/manchester_qbi/manchester_qbi_public/madym_cxx/-/wikis/madym_t1) for step 3.

Each task can be called using a single command, but (for reasons we explain later) we embed each command in it own shell script.

We will be using a [docker image](https://hub.docker.com/repository/docker/michaelberks/madym_xnat) we have created that has the Madym tools installed and ready to use. This docker image has been pulled into our XNAT node as per the main set-up [instructions](setup.md).

We will be running these processing steps on the data for a given XNAT *session* (which remember, belongs to a *subject* that in turn belongs to a *project*).

#### **Step 1. Sorting DICOM data**
So for step 1 our script looks like:

    #!/bin/bash

    #Run madym Dicom convert tool
    $MADYM_ROOT/madym_DicomConvert --config /config/madym_dicom_sort_config.txt "$@"

Here `$MADYM_ROOT` is an environment variable that provides the path to the Madym tools and is defined when Madym is installed, so we don't need to worry about it. 

`--config /config/madym_dicom_sort_config.txt` passes the config file `madym_dicom_sort_config.txt` to the tool, where the `/config` path is a mount we will create to access the *resources folder* of the XNAT project the session belongs to. 

Finally `"$@"` passes any additional command line arguments given to the script as arguments to the Madym tool. This allows us to specify custom options for a given dataset, to override those applied to the whole project set in the config file.

The `madym_dicom_sort_config.txt` file conatins:

    #madym_DicomConvert
    sort = 1
    dicom_dir = dicom_input/SCANS
    output = output/dicom_convert
    audit_dir = output/audit_logs/
    dicom_filter = ""
    dicom_series = dicom_series

  The key points to note here:
  - `dicom_dir = dicom_input/SCANS` this tells the tool where to look for the DICOM images to sort. `SCANS` is the name of the folder where XNAT stores DICOM files for a given session. So at some point we need to create a (read-only) mount point from where the session is located on XNAT's filesystem to `dicom_input` inside the container.

  - `output = output/dicom_convert` this tells the tool where to save output, so we need to create a writable mount so that `output` inside the container points to the *sessions resources* location, where XNAT stores any additional files for a session, outwith the DICOM images (note the `audit_dir` option uses the same location to write out Madym's audit log).

#### **Step 2. Converting DICOM data**
Our shell script for step 2 looks identical to step 1, except we pass as input the config file `madym_dicom_make_config.txt`.

This config file can be found in full [here](madym_config/madym_dicom_make_config.txt), however the key parts to it for this explainer are:

    dicom_dir = dicom_input/SCANS
    output = output/dicom_convert_make
    audit_dir = output/audit_logs/
    dicom_series = session_resources/dicom_convert/dicom_series
    T1_dir = output/vfa
    T1_series = [2 3 4]
    T1_vols = [FA_2deg, FA_10deg, FA_20deg]

- `dicom_dir`, `output` and `audit_dir` are as in step 1

- `dicom_series = session_resources/dicom_convert/dicom_series` tells the tool where to get the series information that was created in step 1. We could achieve this just with the single `output` mount point (in which case the value for this option would be `output/dicom_convert/dicom_series`), but instead, to keep the conceptual separation between input and output data for each step, we will create a read-only mount from the XNAT *session_resources* to the path `session_resources` inside the container.

- The remaining `T1...` options tell the tool to create inputs for T1 mapping from the DICOM series numbers 2, 3 and 4 from the sort stage of step 1, with filenames as specified in `T1_vols` and saved in a folder `vfa` located in the writable output mount.

#### **Step 3: Mapping T1**
Step 3 follows a similar path to step 2. This time our shell script contains the line

    $MADYM_ROOT/madym_T1 --config /config/madym_T1_config.txt "$@"

To call the T1 mapping tool, with config file `madym_T1_config.txt`, with key parts:

    T1_dir = session_resources/vfa
    T1_vols = [FA_2deg_mean.nii.gz,FA_10deg_mean.nii.gz,FA_20deg_mean.nii.gz]
    output = output/T1
    audit_dir = output/audit_logs/

Again we see the same patterns as step 2. The input paths to this step: `session_resources/vfa` match the *output* paths from the previous step, so that now we will be mapping T1 from the NIFTI images created in step 2. The output/audit is again written to the output mount.

#### **Summarizing what we need to define**

To get the above steps running in the container service, we thus need to define:

- The command that will be called when the container starts up, to run one of our shell scripts

- Mount points to the resources of session's parent *project*, as this is where we store

    - The shell scripts we run, which we will mount to `/run` inside the container

    - The config files (so we set a consistent configuration for a whole study), which we mount to `/config` as specified in the main shell script command

- A way for the user to choose which shell script to call (*ie* choosing which of steps 1 - 3 to run)

- A read-only mount point to the DICOM files of the session

- A writable mount point to the session resources, to save output generated at each step

- A read-only mount point to the session resources, so we can use output from previous steps as input to the current step

- A means for setting any additional command-line arguments for each step, so these can passed to the Madym tool, via the shell script

We are now ready to take a deep dive into our JSON file...

### At last... the command JSON file itself
Again, the complete file is [here](xnat_commands/madym_command.json). We break it down step-by-step below:

**Initial settings**

    "name": "madym",
    "label": "madym",
    "description": "Runs a user-selected script to call one of Madyms tools",
    "version": "1.5",
    "schema-version": "1.0",
    "image": "michaelberks/madym_xnat:latest",
    "type": "docker",
    "command-line": "bash /run/#MADYM_RUN_FILE# #ADDITIONAL_MADYM_OPTIONS#",
    "override-entrypoint": true

The first part of the file sets some basic information (name, label, version *etc* - I'll be honest, I'm not entirely sure what `schema-version` does or how important the value is, but all examples I've seen and used, set this as 1). Next come the key fields in this section:

- `"image": "michaelberks/madym_xnat:latest"` defines the docker image we will run (remember you need to have already pulled this image into your XNAT node using the plugin settings page)

- `"type": "docker"` specify we use docker to run our containers

- `"command-line": "bash /run/#MADYM_RUN_FILE# #ADDITIONAL_MADYM_OPTIONS#"` this specifies the command that will run when the container starts, and in this example specifies one of the shell scripts described above, together with any additional options passed to them. Key points to note:

    - `/run` this is a mount point we will create to the project resources folder where we upload our shell scripts to
    - `#MADYM_RUN_FILE#`, `#ADDITIONAL_MADYM_OPTIONS#"` these specify input variables that will instantiated with values set by the user when they call the container command. We will link to these variable in future steps.

- `"override-entrypoint": true` this ensure any existing entry-point commands in the docker image will not run, and our instead overriden by the command above.

**Specifying mounts**

    "mounts": [
      {
        "name": "dicom-input",
        "writable": false,
        "path": "/dicom_input"
      },
      {
        "name": "project-run-scripts",
        "writable": false,
        "path": "/run"
      },
      {
        "name": "project-config",
        "writable": false,
        "path": "/config"
      },
      {
        "name": "session-resources-input",
        "writable": false,
        "path": "/session_resources"
      },
      {
        "name": "session-resources-output",
        "writable": true,
        "path": "/output"
      }
    ]

This section defines each mount we make use of in the container. Each mount takes three options:

- `name` this provides a label we make use of later in the command file, to link XNAT resources to each mount

- `writable` defines whether the mount is read-only or writable

- `path` this defines the (absolute) path the mount will take inside the docker container when it is running.

As you can see, the 5 mounts are those we discussed when detailing the tasks to run:

1. `dicom-input` provides access to the DICOM images stored for a given XNAT session. The images themselves will thus be available on the path `/dicom_input/SCANS` inside the container.

2. `project-run-scripts` provides access to a project resources folder where we will upload our Madym shell scripts. This creates the link to the `/run` path described in the `command-line` setting above.

3. `project-config` provides access to a project resources folder where we will upload our Madym config files. This creates the link to the `/config` path called in the main line of each shell script.

4. `session-resources-input` provides access to the session resources folder where output from previous process steps are stored, enabling them to be used as inputs to the current step.

5. `session-resources-output` provides a writable mount for saving output from each step.

**Variables and ports**

Next are two settings we can leave empty for our tasks

    "environment-variables": {},
    "ports": {},

**Inputs**

Next we specify how the input variables `#MADYM_RUN_FILE#` and `#ADDITIONAL_MADYM_OPTIONS#` are set:

    "inputs": [
      {
        "name": "madym-run-file",
        "description": "Script that will configure and run Madym",
        "type": "string",
        "matcher": null,
        "default-value": null,
        "required": true,
        "replacement-key": "#MADYM_RUN_FILE#",
        "sensitive": null,
        "command-line-flag": null,
        "command-line-separator": null,
        "true-value": null,
        "false-value": null
      },
      {
        "name": "additonal-madym-options",
        "label": null,
        "description": "Additonal options passed to madym command",
        "type": "string",
        "matcher": null,
        "default-value": "",
        "required": false,
        "replacement-key": "#ADDITIONAL_MADYM_OPTIONS#",
        "sensitive": null,
        "command-line-flag": null,
        "command-line-separator": null,
        "true-value": null,
        "false-value": null,
        "select-values": [],
        "multiple-delimiter": null
      }
    ]

Note the two variables are subtly different:

`#MADYM_RUN_FILE#` is not actually linked to a method of setting its input here. However it is now linked to the input construct with name `madym-run-file`, which will be linked to a XNAT object in the next section. This input is also marked as `required: true`.

In contrast this is the last time we need to deal with `#ADDITIONAL_MADYM_OPTIONS#`, the above is sufficient that a free-text entry box will be created in the pop-up window when the user selects to run this container command. The input is marked as `required: false`, because the user can leave the options empty (and indeed usually will).

**Outputs**

This section links the output mount we specified above (with name `session-resources-output`) to an output construct we have named `output`

    "outputs": [
      {
        "name": "output",
        "description": "Output files from Madym analysis",
        "required": true,
        "mount": "session-resources-output",
        "path": null,
        "glob": null
      }
    ]

To complete the output mount, we still need to link this output object to an output handler further below (this is why our command file ends up over 200 lines long!).

**XNAT resources**

This is arguably *the* key section (though of course we need to get them all right for the command to work): it links XNAT resources for the session being processed (and the project it belongs to), to each of the constructs we have specified above. What's more, we specify a dependency between these resources, so that, for example, the project can be automatically derived from the session.

Not the each of the subsections below needs to live within the `xnat` subsection, delineated by square brackets and braces:

    "xnat": [
      {
        ... each of the sections described below ...
      }
    ]

Note you can also have multiple XNAT objects here, so that your command be used in different ways and/or for different XNAT objects (*eg* on a subject instead of a session). However in our setup we just use a single one, and instead allow the user to select from multiple shell scripts to run the different tasks.

    "name": "madym-xnat",
    "label": "Madym-xnat",
    "description": "Run madym_xnat container with a session mounted",
    "contexts": [
      "xnat:imageSessionData"
    ],

The first part provides a name, label and description for this command call. This is what the user will see on the dropdown list of commands they can run for a session. The `contexts` then specifies that this command is only applicable to run on XNAT sessions.


    "external-inputs": [
      {
        "name": "MR-Session",
        "label": null,
        "description": "Input session",
        "type": "Session",
        "matcher": null,
        "default-value": null,
        "required": true,
        "replacement-key": null,
        "sensitive": null,
        "provides-value-for-command-input": null,
        "provides-files-for-command-mount": "dicom-input",
        "via-setup-command": null,
        "user-settable": null,
        "load-children": false
      }
    ],

Next we derive a single external input, this is the session on which our command will run, which now has name `MR-Session`. By setting the `provides-files-for-command-mount` field, we link the DICOM images of the session to the `dicom-input` mount we specified above. The other fields can be left empty/null.

Next we set several derived inputs. We go through these one-by-one, but each must live within the derived inputs subsection:

    "derived-inputs": [
      ... each of the sections described below...
    ]

First, we specify the link to the session resources folder containing output from previous steps.

    {
        "name": "Select-Session-Resource-Directory",
        "description": "Session resource containing results of madym processing",
        "type": "Resource",
        "matcher": null,
        "default-value": null,
        "required": false,
        "replacement-key": null,
        "sensitive": null,
        "provides-value-for-command-input": null,
        "provides-files-for-command-mount": "session-resources-input",
        "user-settable": true,
        "load-children": true,
        "derived-from-wrapper-input": "MR-Session",
        "derived-from-xnat-object-property": null,
        "via-setup-command": null
    },

This has `type` `Resource`, and using the `derived-from-wrapper-input` field, is derived from the `MR-Session` external input defined above. Again we set the `provides-files-for-command-mount` field, this time to provide files for the `session-resources-input` mount.

Next we link to the parent project:

    {
        "name": "Project",
        "description": "Specific project this Session is in",
        "type": "Project",
        "matcher": null,
        "default-value": null,
        "required": true,
        "replacement-key": null,
        "sensitive": null,
        "provides-value-for-command-input": null,
        "provides-files-for-command-mount": null,
        "user-settable": null,
        "load-children": true,
        "derived-from-wrapper-input": "MR-Session",
        "derived-from-xnat-object-property": null,
        "via-setup-command": null
    },

Here, we just provide a name for this input, `Project`, set it's type (also as `Project`), and set the `derived-from-wrapper-input` field, so again it will be automatically derived from the `MR-Session`. This is *great*, because it means a user can't make the mistake of loading the wrong config files to process a dataset - simply by choosing a session to process, XNAT will link to the correct set of project-wide config files. We also set `load-children` to be true, because we want the project resource folders available for the derived inputs we look at next.

Next we make use of the `Project` object, by deriving two further inputs from it. Firstly, the project resources folder where the run scripts are:

    {
        "name": "Select-Project-Run-Scripts-Directory",
        "description": "Project resource containing madym config files",
        "type": "Resource",
        "matcher": null,
        "default-value": "madym_run",
        "required": true,
        "replacement-key": null,
        "sensitive": null,
        "provides-value-for-command-input": null,
        "provides-files-for-command-mount": "project-run-scripts",
        "user-settable": false,
        "load-children": true,
        "derived-from-wrapper-input": "Project",
        "derived-from-xnat-object-property": null,
        "via-setup-command": null
      },

Secondly the project resources folder where the config files are:

    {
      "name": "Select-Project-Config-Directory",
      "description": "Project resource containing madym config files",
      "type": "Resource",
      "matcher": null,
      "default-value": "madym_config",
      "required": true,
      "replacement-key": null,
      "sensitive": null,
      "provides-value-for-command-input": null,
      "provides-files-for-command-mount": "project-config",
      "user-settable": false,
      "load-children": true,
      "derived-from-wrapper-input": "Project",
      "derived-from-xnat-object-property": null,
      "via-setup-command": null
    }
    ,

Note we set each of these to be `user-settable` false, and instead provide a default value, `madym_run` and `madym_config` respectively. These names *must* match the names of the folders you create using `Manage files` page of the project. Again, we make use of the `provides-files-for-command-mount` field, so that these resource folders are linked to `project-run-scripts` and `project-config` mounts we specified above.

Finally we have two inputs so the user can interactively select the specific step they want to run.

Firstly, we derive an input object from the `Select-Project-Run-Scripts-Directory` input, setting `load-children` to be true, so we get the list of files inside the project resource folder containing the run scripts, from which the user will select a script to run.

    {
      "name": "Select-Run-File",
      "description": "Madym run file",
      "type": "File",
      "matcher": null,
      "default-value": null,
      "required": true,
      "replacement-key": null,
      "sensitive": null,
      "provides-value-for-command-input": null,
      "provides-files-for-command-mount": null,
      "user-settable": true,
      "load-children": true,
      "derived-from-wrapper-input": "Select-Project-Run-Scripts-Directory",
      "derived-from-xnat-object-property": null,
      "via-setup-command": null
    },

Finally, we define an input that links this selection to the `madym-run-file` input we defined above, which in turn instantiates the `#MADYM_RUN_FILE#` variable passed to the main container command. Note this has `user-settable` false, because it is the `Select-Run-File` input object above where the actual user selection occurs.

    {
      "name": "Madym-Run-File",
      "description": null,
      "type": "string",
      "matcher": null,
      "default-value": null,
      "required": true,
      "replacement-key": null,
      "sensitive": null,
      "provides-value-for-command-input": "madym-run-file",
      "provides-files-for-command-mount": null,
      "user-settable": false,
      "load-children": false,
      "derived-from-wrapper-input": "Select-Run-File",
      "derived-from-xnat-object-property": "name",
      "via-setup-command": null
    }

The final part of the XNAT resources section specifies an output handler, so that our `output` object created above gets linked to the session resources. Without this section, the output mount we created above never actually gets saved to the permanent part of the XNAT filesystem. The value assigned to the `label` field here (in this case `MADYM_OUTPUT`), will be the name of the resource folder created for the session. 

    "output-handlers": [
        {
          "name": "output-resource",
          "accepts-command-output": "output",
          "via-wrapup-command": null,
          "as-a-child-of": "MR-Session",
          "type": "Resource",
          "label": "MADYM_OUTPUT",
          "format": null,
          "description": null,
          "content": null,
          "tags": []
        }
      ]

**And we're done!**
Well almost... there's a couple more things we can leave blank:

    "container-labels": {},
    "generic-resources": {},
    "ulimits": {}

Phew.. now we really have made it!

You are now ready to call your docker container from the browser interface of your XNAT node. When you select a session, the command name (the first field of the **XNAT resources** section) will be available as option in the `Run containers` section of the `Actions` menu. When this is selected, a pop-up window will show the user the set of options we have described above. Between automatically derived inputs and default values, the only options the user has to select are:

1. The run script they want to call
2. Optionally, any additional options they want to set for this command

## Summary
Above, we listed the items we needed to specify in the command file move from running our docker file locally, to enabling it as a command on the container service that will be called by a user via the XNAT browser interface. We return to this list, summarising how each option gets set in the command file.

- The command that will be called when the container starts up, to run one of our shell scripts: set using the `command-line` field at the start of the JSON file. This command takes as variables `#MADYM_RUN_FILE#` and `#ADDITIONAL_MADYM_OPTIONS#` that are set further on.

- Mount points to the resources of session's parent *project*, as this is where we store

    - The shell scripts we run, which we will mount to `/run` inside the container: this is created by specifying a mount with name `project-run-scripts` in the mount section, which in turn is linked using the `provides-files-for-command-mount` field of the `Select-Project-Run-Scripts-Directory` XNAT input, derived from the `Project` XNAT input, itself derived from the `MR-Session` XNAT input.

    - The config files (so we set a consistent configuration for a whole study), which we mount to `/config` as specified in the main shell script command: this is created by specifying a mount with name `project-config` in the mount section, which in turn is linked using the `provides-files-for-command-mount` field of the `Select-Project-Config-Directory` XNAT input, derived from the `Project` XNAT input, itself derived from the `MR-Session` XNAT input.

- A way for the user to choose which shell script to call (*ie* choosing which of steps 1 - 3 to run): this is created by deriving an XNAT input `Select-Run-File` from the previously derived `Select-Project-Run-Scripts-Directory`. This provides a dropdown list of the files in the resources folder for the user to choose from. Their selection is saved in an XNAT input `Madym-Run-File`, that, using the `provides-value-for-command-input` field is linked to the top-level input `madym-run-file`, which in turn is linked to the variable `#MADYM_RUN_FILE#` in the main container command.

- A read-only mount point to the DICOM files of the session: this is created by specifying a mount with name `dicom-input` in the mount section, which in turn is linked using the `provides-files-for-command-mount` field of the `MR-Session` XNAT input.

- A writable mount point to the session resources, to save output generated at each step: this is created by specifying a mount with name `session-resources-output` in the mount section, which in turn is linked to an `output` object, which in turn is linked to an XNAT session output handler so that a folder `MADYM_OUTPUT` will be created in the session resources.

- A read-only mount point to the session resources, so we can use output from previous steps as input to the current step: this is created by specifying a mount with name `session-resources-output` in the mount section, which in turn is linked to an XNAT session resources folder  using the `provides-files-for-command-mount` field of the `Select-Session-Resource-Directory` XNAT input, derived from `MR-Session` XNAT input.

- A means for setting any additional command-line arguments for each step, so these can passed to the Madym tool, via the shell script: this is created by specifying an input `additional-madym-options` that is linked, by setting the `replacement_key` field, to the variable `#ADDITIONAL_MADYM_OPTIONS#` used in the main container command line.








