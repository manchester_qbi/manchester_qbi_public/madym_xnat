series_number = 0
DCMCatalog_UID = 0
project = 0
datestamp = '29:Tue Feb 15 21:45:16 UTC 2022'
slice_datestamp = '2022-02-15T21:45:16.703'
createdEventId = 29
creator = 'admin'
slice_IDs = [
    ('1.3.46.670589.11.22117.5.0.1560.2009120213165950016-1201-6-195xh2d.dcm', '1.3.46.670589.11.22117.5.0.6228.2009120213340110721', '81a93cb5c0d9f39d930d96ab5c7eb0fd', 1),
    ('1.3.46.670589.11.22117.5.0.1560.2009120213165950016-1201-15-195dofe.dcm', '1.3.46.670589.11.22117.5.0.6228.2009120213340100705', 'e7d75ee68956ffe63f6e95a8da92af0c', 15)
]
num_added = len(slice_IDs)
nx = 128
ny = 128
nz = 25
dx = 2.9296875
dy = 2.9296875
dz = 8.0
orientation = 'Tra'

scan_xml_file = f'scan_{series_number}_catalog.xml'

with open(scan_xml_file, 'wt') as xml_fid:
    print(f'<?xml version="1.0" encoding="UTF-8"?>', file=xml_fid)
    print(f'<cat:DCMCatalog UID="{DCMCatalog_UID}" xmlns:arc="http://nrg.wustl.edu/arc" xmlns:cat="http://nrg.wustl.edu/catalog" xmlns:icr="http://icr.ac.uk/icr" xmlns:pipe="http://nrg.wustl.edu/pipe" xmlns:prov="http://www.nbirn.net/prov" xmlns:scr="http://nrg.wustl.edu/scr" xmlns:val="http://nrg.wustl.edu/val" xmlns:wrk="http://nrg.wustl.edu/workflow" xmlns:xdat="http://nrg.wustl.edu/security" xmlns:xnat="http://nrg.wustl.edu/xnat" xmlns:xnat_a="http://nrg.wustl.edu/xnat_assessments" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">', file=xml_fid)
    print(f'<cat:metaFields>', file=xml_fid)
    print(f'    <cat:metaField name="PROJECT">{project}</cat:metaField>', file=xml_fid)
    print(f'    <cat:metaField name="AUDIT">{datestamp}=Added:25</cat:metaField>', file=xml_fid)
    print(f'</cat:metaFields>', file=xml_fid)
    print(f'<cat:entries>', file=xml_fid)
    for slice in slice_IDs:
        print(f'    <cat:entry ID="{slice[0]}" UID="{slice[1]}" URI="{slice[0]}" createdBy="{creator}" createdEventId="{createdEventId}" createdTime="{slice_datestamp}" digest="{slice[2]}" format="DICOM" instanceNumber="{slice[3]}" xsi:type="cat:dcmEntry"/>', file=xml_fid)

    print(f'</cat:entries>', file=xml_fid)
    print(f'<cat:dimensions x="{nx}" y="{ny}" z="{nz}"/>', file=xml_fid)
    print(f'<cat:voxelRes x="{dx}" y="{dy}" z="{dz}"/>', file=xml_fid)
    print(f'<cat:orientation>{orientation}</cat:orientation>', file=xml_fid)
    print(f'</cat:DCMCatalog>', file=xml_fid)