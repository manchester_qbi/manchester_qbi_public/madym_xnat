# Setting up an XNAT instance from scratch
This describes the process we have used to set-up a local XNAT instance running on the QBI lab's mac-mini. We have not tested on other systems, but the general steps should be broadly similar.

## Overview

1. Install pre-requisites Virtual Box and Vagrant
2. Clone the `xnat-vagrant` git repository from GitHub, and run the set-up script
3. Start the xnat instance and then SSH into it
4. Clone the `container-service` git repository from BitBucket
5. Build the container service using the bundle gradle tool
6. Copy the container service jar file to the plugins directory

## Details

### Getting an XNAT instance running
To get an instance of xnat installed and running, we follow the instruction [here](https://wiki.xnat.org/documentation/getting-started-with-xnat/running-xnat-in-a-vagrant-virtual-machine).

First install [Vagrant](https://www.vagrantup.com) and [Virtual Box](https://www.virtualbox.org). In our tests we are running Vagrant 2.2.19 and Virtual Box 6.1.22.

Next, open a command terminal and clone the [xnat-vagrant GitHub repository](https://bitbucket.org/xnatdev/xnat-vagrant.git)
> git clone --branch master https://bitbucket.org/xnatdev/xnat-vagrant.git

CD into `xnat-vagrant` and run
> ./run xnat setup

This should begin setup, which will take several minutes to complete. You will need a solid internet connection as if it cuts out, you will get a confusing mess of errors reported and the set-up will not complete.

SSH into the virtual machine
    ./run xnat ssh

Open a browswer window, enter address `http://10.1.1.17`. If you see Bad gateway 502... restart tomcat.

Enter `admin` for both username and password.

### Create a new project

### Restarting tomcat

    sudo service tomcat9 stop
    sudo service tomcat9 start

Starting tomcat when the virtual machine starts

    sudo systemctl enable tomcat9.service

### Setting up the container plugin

    git clone https://bitbucket.org/xnatdev/container-service.git
    cd container-service
    ./gradlew fatJar
    sudo cp /home/vagrant/container-service/build/libs/container-service-3.1.0-fat.jar /data/xnat/home/plugins/

Restart tomcat as per instructions above.

Now log-in to xnat via a browser window. If the container service plugin has worked, when you click on Administer in the top menu bar one of the items in the pop-up menu (in our case the 3rd item) will be Plugin settings. Click on this link to take you to the plugin settings page.

In theory, we no longer need the SSH terminal, as all the remaining tasks are done through the xnat browser interface. However it is useful to keep this open, to explore the xnat file system as we load data and run some test container commands

### Adding images to the container plugin
On the left-hand side of the plugin settings screen, select `Images and commands`, then click the `Add New Image` button. In the pop-up window that opens, set the `Image name` as `michaelberks/protocol-compliance`. You can leave the version box empty, then click the `Pull Image` button. This will take a few seconds (depending on your internet speed) to pull the docker image from DockerHub. The protocol compliance image should now appear under the `Installed Container Images and Commands` in the main part of the plugin page. The image comes preloaded with a command suitable for running on xnat. This is labelled 'Protocol compliance' under the command column of the image. If you click on this link, the command pops up in a new editor window, and can be edited/inspected directly. Each image can have multiple commands, however we will only use one per image.

Now click `Add New Image` again, and this time pull `michaelberks/madym_xnat`.  

Select `Command configurations` from the left hand menu of the plugin settings page. The commands associated with each image appear in the main part of the screen. Click the checkbox under the `Enabled` column to enable both commands.

In addition to enabling the commands in the plugin settings, each command needs to be enabled for any project we want to run it in. Click `Browse -> My projects` on the top menu, and select the `QBI NCITA test` project we created earlier. On the right-hand side of the project page, under `Actions`, click on `Project settings`. This should link to a page, where the `Configure commands` tab is already open, with the a table similar to the one in plugin settings page showing each command, with a checkbox to the enable them. Note only commands that have been enabled on the main plugins page can be enabled here, otherwise the command will be greyed out on the project settings page.

### Add config commands to the project resources

### Adding some test data

On your local computer, install the `XNAT-Desktop-Client` from [here](https://wiki.xnat.org/xnat-tools/xnat-desktop-client-dxm/installing-the-xnat-desktop-client)

`Actions -> Add -> Subject` enter subject ID `subject001`. You can leave all other fields blank.

Subject page created.

Under actions, select `Upload images`. This should automatically open the `XNAT-Desktop-Client`, which has a simple interface for selecting and uploading DICOM images from your local machine. When you have selected the images to upload, they will be sorted into their separate series, and you have the option to preview them. When you are happy, click proceed and the upload to xnat will begin. This will take several minutes to complete, depending on how much data you have selected.




